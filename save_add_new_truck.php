<?php
require_once("connect.php");

$timestamp=date("Y-m-d H:i:s");
$tno=escapeString($conn,strtoupper($_POST['tno']));
$truck_type=escapeString($conn,strtoupper($_POST['truck_type']));
$company=escapeString($conn,strtoupper($_POST['company']));
$wheeler=escapeString($conn,strtoupper($_POST['wheeler']));
$model=escapeString($conn,strtoupper($_POST['model']));
$fastag_acno=escapeString($conn,strtoupper($_POST['fastag_acno']));
$fastag_srno=escapeString($conn,strtoupper($_POST['fastag_srno']));
$supervisor=escapeString($conn,strtoupper($_POST['supervisor']));
$body_height=escapeString($conn,strtoupper($_POST['body_height']));
$body_width=escapeString($conn,strtoupper($_POST['body_width']));
$body_length=escapeString($conn,strtoupper($_POST['body_length']));
$body_floor_to_top=escapeString($conn,strtoupper($_POST['body_floor_to_top']));
$body_type=escapeString($conn,strtoupper($_POST['body_type']));
$axle_count=escapeString($conn,strtoupper($_POST['axle_count']));
$axle_company=escapeString($conn,strtoupper($_POST['axle_company']));
$axle_bolt_qty=escapeString($conn,strtoupper($_POST['axle_bolt_qty']));
$bought_on=escapeString($conn,strtoupper($_POST['bought_on']));
$on_road_on=escapeString($conn,strtoupper($_POST['on_road_on']));
$empty_weight=escapeString($conn,strtoupper($_POST['empty_weight']));
$bought_from=escapeString($conn,strtoupper($_POST['bought_from']));
$body_created_by=escapeString($conn,strtoupper($_POST['body_created_by']));
$chasis_no=escapeString($conn,strtoupper($_POST['chasis_no']));
$engine_no=escapeString($conn,strtoupper($_POST['engine_no']));
$gear_no=escapeString($conn,strtoupper($_POST['gear_no']));
$cabin_no=escapeString($conn,strtoupper($_POST['cabin_no']));

$fix_name=$tno."_".date('dmYHis').mt_rand();

$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");

if(!in_array($_FILES['chasis_img']['type'], $valid_types))
{
	echo "<script>alert('Error : Only Image Upload Allowed : CHASIS NO. IMAGE');</script>";exit();
}

if(!in_array($_FILES['engine_img']['type'], $valid_types))
{
	echo "<script>alert('Error : Only Image Upload Allowed : ENGINE NO. IMAGE');</script>";exit();
}

if(!in_array($_FILES['gear_img']['type'], $valid_types))
{
	echo "<script>alert('Error : Only Image Upload Allowed : GEAR NO. IMAGE');</script>";exit();
}

if(!in_array($_FILES['cabin_img']['type'], $valid_types))
{
	echo "<script>alert('Error : Only Image Upload Allowed : CABIN NO. IMAGE');</script>";exit();
}

$sourcePath_Chasis = $_FILES['chasis_img']['tmp_name'];
$sourcePath_Engine = $_FILES['engine_img']['tmp_name'];
$sourcePath_Gear = $_FILES['gear_img']['tmp_name'];
$sourcePath_Cabin = $_FILES['cabin_img']['tmp_name'];

$targetPath_Chasis = "chasis_img/".$fix_name.".".pathinfo($_FILES['chasis_img']['name'],PATHINFO_EXTENSION);
$targetPath_Engine = "engine_img/".$fix_name.".".pathinfo($_FILES['engine_img']['name'],PATHINFO_EXTENSION);
$targetPath_Gear = "gear_img/".$fix_name.".".pathinfo($_FILES['gear_img']['name'],PATHINFO_EXTENSION);
$targetPath_Cabin = "cabin_img/".$fix_name.".".pathinfo($_FILES['cabin_img']['name'],PATHINFO_EXTENSION);

$maxDimW_Chasis = "1500";
$maxDimH_Chasis = "1500";
$maxDimW_Engine = "1500";
$maxDimH_Engine = "1500";
$maxDimW_Gear = "1500";
$maxDimH_Gear = "1500";	
$maxDimW_Cabin = "1500";
$maxDimH_Cabin = "1500";

include ('./up_func_gear.php');
include ('./up_func_chasis.php');
include ('./up_func_engine.php');
include ('./up_func_cabin.php');

if(!move_uploaded_file($sourcePath_Chasis, $targetPath_Chasis)) 
{
	echo "<script>alert('Unable to upload Chasis No File.');</script>";exit();
}

if(!move_uploaded_file($sourcePath_Engine, $targetPath_Engine)) 
{
	unlink($targetPath_Chasis);
	echo "<script>alert('Unable to upload Engine No File.');</script>";exit();
}

if(!move_uploaded_file($sourcePath_Gear, $targetPath_Gear)) 
{
	unlink($targetPath_Engine);
	unlink($targetPath_Chasis);
	echo "<script>alert('Unable to upload Gear No File.');</script>";exit();
}

if(!move_uploaded_file($sourcePath_Cabin, $targetPath_Cabin)) 
{
	unlink($targetPath_Engine);
	unlink($targetPath_Chasis);
	unlink($targetPath_Gear);
	echo "<script>alert('Unable to upload Cabin Number File.');</script>";exit();
}

if(count($_FILES['vehicle_pic']['name'])>0)
{
	foreach($_FILES['vehicle_pic']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			echo "<script>alert('Error : Only Image Upload Allowed.');</script>";exit();
		}
    }
	
	for($i=0; $i<count($_FILES['vehicle_pic']['name']);$i++) 
	{
		
	$sourcePath = $_FILES['vehicle_pic']['tmp_name'][$i];
	$targetPath = "vehicle_pic/".$fix_name.".".pathinfo($_FILES['vehicle_pic']['name'][$i],PATHINFO_EXTENSION);
	$maxDimW = "1500";
	$maxDimH = "1500";
	include ('./up_func.php');

	if(move_uploaded_file($sourcePath, $targetPath)) 
	{
		$vehicle_pics[] = $targetPath;
	}
	else
	{
		unlink($targetPath_Engine);
		unlink($targetPath_Chasis);
		unlink($targetPath_Gear);
		unlink($targetPath_Cabin);
		echo "<script>alert('Unable to upload Vehicle Pictures.');</script>";exit();
	}
	
	}
	
	$veh_photos=implode(',',$vehicle_pics);
}
else
{
		unlink($targetPath_Engine);
		unlink($targetPath_Chasis);
		unlink($targetPath_Gear);
		unlink($targetPath_Cabin);
	echo "<script>alert('Please Upload Vehicle Pictures.');</script>";exit();	
}

$insert=Qry($conn,"INSERT INTO dairy.own_truck(tno,ediary_active,comp,wheeler,model,tag_acno,tag_srno,superv_id,truck_type,height,width,length,
floor_to_top,body_type,axle_count,axle_comp,axle_bolt_qty,chasis_no,chasis_file,engine_no,engine_file,gear_no,gear_file,cabin_no,
cabin_file,bought_on,on_road_on,empty_weight,veh_pics,bought_from,body_created_by,timestamp) VALUES ('$tno','1','$company','$wheeler',
'$model','$fastag_acno','$fastag_srno','$supervisor','$truck_type','$body_height','$body_width','$body_length','$body_floor_to_top',
'$body_type','$axle_count','$axle_company','$axle_bolt_qty','$chasis_no','$targetPath_Chasis','$engine_no','$targetPath_Engine','$gear_no',
'$targetPath_Gear','$cabin_no','$targetPath_Cabin','$bought_on','$on_road_on','$empty_weight','$veh_photos','$bought_from','$body_created_by',
'$timestamp')");

if(!$insert)
{
	echo mysqli_errno($conn);exit();
}

$insert_rrpl_db=Qry($conn,"INSERT INTO own_truck(tno,comp) VALUES ('$tno','$company')");

if(!$insert_rrpl_db)
{
	echo mysqli_errno($conn);exit();
}

$insert2=Qry($conn,"INSERT INTO own_truck_docs(tno) VALUES ('$tno')");
if(!$insert2)
{
	echo mysqli_errno($conn);exit();
}

$insert3=Qry($conn,"INSERT INTO own_truck_docs_exp(tno) VALUES ('$tno')");
if(!$insert3)
{
	echo mysqli_errno($conn);exit();
}

echo "<script>alert('Truck Number : $tno Registered Successfully !'); document.getElementById('TruckForm').reset();</script>";exit();
?>