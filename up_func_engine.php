<?php 
list($width, $height, $type, $attr) = getimagesize($sourcePath_Engine);
if ( $width > $maxDimW_Engine || $height > $maxDimH_Engine ) {
    $target_filename = $sourcePath_Engine;
    $fn = $sourcePath_Engine;
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDimW_Engine;
        $height = $maxDimH_Engine/$ratio;
    } else {
        $width = $maxDimW_Engine*$ratio;
        $height = $maxDimH_Engine;
    }
    $src = @imagecreatefromstring(@file_get_contents(@$fn));
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );

    imagejpeg($dst, $target_filename); // adjust format as needed
}
?>