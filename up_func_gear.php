<?php 
list($width, $height, $type, $attr) = getimagesize($sourcePath_Gear);
if ( $width > $maxDimW_Gear || $height > $maxDimH_Gear ) {
    $target_filename = $sourcePath_Gear;
    $fn = $sourcePath_Gear;
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDimW_Gear;
        $height = $maxDimH_Gear/$ratio;
    } else {
        $width = $maxDimW_Gear*$ratio;
        $height = $maxDimH_Gear;
    }
    $src = @imagecreatefromstring(@file_get_contents(@$fn));
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );

    imagejpeg($dst, $target_filename); // adjust format as needed
}
?>