<?php
require_once("./connect.php");

$tno = escapeString($conn,strtoupper($_POST['tno']));
$timestamp = date("Y-m-d H:i:s");

if($tno=='')
{
	echo "<script type='text/javascript'> 
		alert('Error : Unable to Get Truck Number.');
	</script>";
	exit();
}

// dates 

$puc_from = escapeString($conn,$_POST['puc_from']);
$puc_to = escapeString($conn,$_POST['puc_to']);
$permit_one_from = escapeString($conn,$_POST['permit_1_from']);
$permit_one_to = escapeString($conn,$_POST['permit_1_to']);
$permit_five_from = escapeString($conn,$_POST['permit_5_from']);
$permit_five_to = escapeString($conn,$_POST['permit_5_to']);
$fitness_date_from = escapeString($conn,$_POST['fitness_from']);
$fitness_date_to = escapeString($conn,$_POST['fitness_to']);
$ins_date_from = escapeString($conn,$_POST['ins_from']);
$ins_date_to = escapeString($conn,$_POST['ins_to']);

// dates 


if($_POST['p_5_na']=='NA')
{
	$permit_five_from="";
	$permit_five_to="";
	$sourcePath_permit_five = "";
}
else
{
	$sourcePath_permit_five = $_FILES['permit_copy_5']['tmp_name'];
}

if($_POST['fitness_na']=='NA')
{
	$fitness_date_from="";
	$fitness_date_to="";
	$sourcePath_fitness="";
}
else
{
	$sourcePath_fitness = $_FILES['fitness']['tmp_name']; // UPLOAD FILE
}

if($_POST['ins_na']=='NA')
{
	$ins_date_from="";
	$ins_date_to="";
	$sourcePath_ins="";
}
else
{
	$sourcePath_ins = $_FILES['ins']['tmp_name']; // UPLOAD FILE
}

if($_POST['tax_na']=='NA')
{
	$tax_date_from = '';
	$tax_date_to = '';
	$sourcePath_tax="";
}
else
{
	if($_POST['tax_type']=='0')
	{
		$tax_date_from = escapeString($conn,$_POST['tax_from']);
		$tax_date_to = escapeString($conn,$_POST['tax_to']);
		$targetPath_tax = "tax_one/".$tno.mt_rand().".".pathinfo($_FILES["tax"]["name"],PATHINFO_EXTENSION);	
		$tax_col = "tax";
	}
	else
	{
		$tax_date_from = '';
		$tax_date_to = '';
		$targetPath_tax = "tax_lifetime/".$tno.mt_rand().".".pathinfo($_FILES["tax"]["name"],PATHINFO_EXTENSION);
		$tax_col = "tax_lifetime";	
	}
	
	$sourcePath_tax = $_FILES['tax']['tmp_name'];
}

//SOURCE FILES
$sourcePath_rc_front = $_FILES['rc_front']['tmp_name']; // RC FRONT
$sourcePath_rc_rear = $_FILES['rc_rear']['tmp_name']; // RC REAR
$sourcePath_puc = $_FILES['puc']['tmp_name']; // PUC
$sourcePath_permit_one = $_FILES['permit_copy_1']['tmp_name'];

if($sourcePath_puc!='' && ($puc_from=='' || $puc_to==''))
{
	echo "<script type='text/javascript'> 
		alert('PUC dates are required while uploading PUC.');
		$('#submit_docs').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if($sourcePath_puc=='')
{
	$puc_from='';
	$puc_to='';
}

if($sourcePath_permit_one!='' && ($permit_one_from=='' || $permit_one_to==''))
{
	echo "
	<script type='text/javascript'> 
		alert('Permit-1Yr dates are required while uploading Permit-1Yr !');
		$('#submit_docs').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>
	";
	exit();
}

if($sourcePath_permit_one=='')
{
	$permit_one_from='';
	$permit_one_to='';
}

$targetPath_rc_front = "rc_front/".$tno.mt_rand().".".pathinfo($_FILES["rc_front"]["name"],PATHINFO_EXTENSION);
$targetPath_rc_rear = "rc_rear/".$tno.mt_rand().".".pathinfo($_FILES["rc_rear"]["name"],PATHINFO_EXTENSION);

//TARGET FILES TO SAVE

if(pathinfo($_FILES["rc_front"]["name"],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1000,1000,$sourcePath_rc_front);
}

if(pathinfo($_FILES["rc_rear"]["name"],PATHINFO_EXTENSION)!='pdf')
{
	ImageUpload(1200,1200,$sourcePath_rc_rear);
}

if(@pathinfo($_FILES["puc"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_FILES['puc']['name']!='')
{
	ImageUpload(1200,1200,$sourcePath_puc);
}

if(@pathinfo($_FILES["fitness"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_POST['fitness_na']!='NA' AND $_POST['fitness_na']!='')
{
	ImageUpload(1200,1200,$sourcePath_fitness);
}

if(@pathinfo($_FILES["ins"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_POST['ins_na']!='NA' AND $_POST['ins_na']!='')
{
	ImageUpload(1200,1200,$sourcePath_ins);
}

if(@pathinfo($_FILES["permit_copy_1"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_FILES['permit_copy_1']['name']!='')
{
	ImageUpload(1200,1200,$sourcePath_permit_one);
}

if(@pathinfo($_FILES["permit_copy_5"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_POST['p_5_na']!='NA' AND $_POST['p_5_na']!='')
{
	ImageUpload(1200,1200,$sourcePath_permit_five);
}

if(@pathinfo($_FILES["tax"]["name"],PATHINFO_EXTENSION)!='pdf' AND $_POST['tax_na']!='NA' AND $_POST['tax_na']!='')
{
	ImageUpload(1200,1200,$sourcePath_tax);
}

StartCommit($conn);
$flag = true;

if($sourcePath_puc!='')
{
	$targetPath_puc = "puc/".$tno.mt_rand().".".pathinfo($_FILES["puc"]["name"],PATHINFO_EXTENSION);

	if(!move_uploaded_file($sourcePath_puc,$targetPath_puc))
	{
		$flag = false;
	}
}
else
{
	$targetPath_puc = 'NA';
}

if($sourcePath_permit_one!='')
{
	$targetPath_per_one = "permit_one/".$tno.mt_rand().".".pathinfo($_FILES["permit_copy_1"]["name"],PATHINFO_EXTENSION);
	
	if(!move_uploaded_file($sourcePath_permit_one,$targetPath_per_one))
	{
		$flag = false;
	}
}
else
{
	$targetPath_per_one='NA';
}

if($sourcePath_rc_front!='')
{	
	if(!move_uploaded_file($sourcePath_rc_front,$targetPath_rc_front))
	{
		$flag = false;
	}
}
else
{
	$targetPath_rc_front="NA";
}

if($sourcePath_rc_rear!='')
{ 
	if(!move_uploaded_file($sourcePath_rc_rear,$targetPath_rc_rear))
	{
		$flag = false;
	}
}
else
{
	$targetPath_rc_rear="NA";
}

if($sourcePath_permit_five!='')
{
	$targetPath_per_five = "permit_five/".$tno.mt_rand().".".pathinfo($_FILES["permit_copy_5"]["name"],PATHINFO_EXTENSION);

	if(!move_uploaded_file($sourcePath_permit_five,$targetPath_per_five))
	{
		$flag = false;
	}
}
else
{
	$targetPath_per_five="NA";
}

if($sourcePath_fitness!='')
{
	$targetPath_fitness = "fitness/".$tno.mt_rand().".".pathinfo($_FILES["fitness"]["name"],PATHINFO_EXTENSION);
	
	if(!move_uploaded_file($sourcePath_fitness,$targetPath_fitness))
	{
		$flag = false;
	}
}
else
{
	$targetPath_fitness="NA";
}

if($sourcePath_tax!='')
{
	if(!move_uploaded_file($sourcePath_tax,$targetPath_tax))
	{
		$flag = false;
	}
}
else
{
	$targetPath_tax="NA";
}

if($sourcePath_ins!='')
{
	$targetPath_ins = "ins/".$tno.mt_rand().".".pathinfo($_FILES["ins"]["name"],PATHINFO_EXTENSION);
	
	if(!move_uploaded_file($sourcePath_ins,$targetPath_ins))
	{
		$flag = false;
	}
}
else
{
	$targetPath_ins="NA";
}

// echo $targetPath_rc_front."<br>";
// echo $targetPath_rc_rear."<br>";
// echo $targetPath_puc."<br>";
// echo $targetPath_per_one."<br>";
// echo $targetPath_per_five."<br>";
// echo $targetPath_fitness."<br>";
// echo $targetPath_tax."<br>";
// echo $targetPath_ins."<br>";

// echo "<script> 	
		// $('#submit_docs').attr('disabled',false);
		// $('#loadicon').fadeOut('slow');
	// </script>";
	// exit();
	

if($_POST['tax_na']!='NA')
{
	$update_file = Qry($conn,"UPDATE own_truck_docs SET rc_front='$targetPath_rc_front',rc_rear='$targetPath_rc_rear',
	puc='$targetPath_puc',permit_one='$targetPath_per_one',permit_five='$targetPath_per_five',fitness='$targetPath_fitness',
	`$tax_col`='$targetPath_tax',ins='$targetPath_ins',timestamp='$timestamp' WHERE tno='$tno'");

	if(!$update_file){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}
else
{
	$update_file = Qry($conn,"UPDATE own_truck_docs SET rc_front='$targetPath_rc_front',rc_rear='$targetPath_rc_rear',
	puc='$targetPath_puc',permit_one='$targetPath_per_one',permit_five='$targetPath_per_five',fitness='$targetPath_fitness',
	ins='$targetPath_ins',timestamp='$timestamp' WHERE tno='$tno'");

	if(!$update_file){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}	
					
$update_dates = Qry($conn,"UPDATE own_truck_docs_exp SET permit_five_start='$permit_five_from',
										permit_five_end='$permit_five_to',
										permit_one_start='$permit_one_from',
										permit_one_end='$permit_one_to',
										fitness_start='$fitness_date_from',
										fitness_end='$fitness_date_to',
										tax_start='$tax_date_from',
										tax_end='$tax_date_to',
										ins_start='$ins_date_from',
										ins_end='$ins_date_to',
										puc_start='$puc_from',
										puc_end='$puc_to'
										WHERE tno='$tno'");
	

if(!$update_dates){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Documents Uploaded Successfully.');
		// window.location.href='./upload.php';
		$('#submit_docs').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
		$('#get_button')[0].click();
	</script>";
	exit();
}
else
{
	if($sourcePath_rc_front!=''){
		unlink($targetPath_rc_front);
	}
	
	if($sourcePath_rc_rear!=''){
		unlink($targetPath_rc_rear);
	}
	
	if($sourcePath_puc!=''){
		unlink($targetPath_puc);
	}
	
	if($sourcePath_permit_one!=''){
		unlink($targetPath_per_one);
	}
	
	if($sourcePath_permit_five!=''){
		unlink($targetPath_per_five);
	}
	
	if($sourcePath_fitness!=''){
		unlink($targetPath_fitness);
	}
	
	if($sourcePath_tax!=''){
		unlink($targetPath_tax);
	}
	
	if($sourcePath_ins!=''){
		unlink($targetPath_ins);
	}
	
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script> 	
		alert('Error While Processing Request !');
		$('#submit_docs').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
?>