<?php
require_once("connect.php");

$tno=$_POST['tno'];
$type=$_POST['type'];

$fix_name=$tno.date('dmYHis').mt_rand();

if($tno=="")
{
	echo "<script>
			alert('ERROR : Unable to fetch Truck Number.');
		</script>";
	exit();
}

if($type=='INS')
{
	$folder_name="ins";
	$file="ins_copy";
	$file_col="ins";
	$date_from_col="ins_start";
	$date_to_col="ins_end";
	$date_from=$_POST['ins_from'];
	$date_to=$_POST['ins_to'];
}
else if($type=='TAX')
{
	if($_POST['tax_type1']=='0')
	{
		$type='TAX_ONCE';
		
		$file_col="tax";
		$date_from=$_POST['tax_from'];
		$date_to=$_POST['tax_to'];
		
		if($date_from=="" || $date_to=="")
		{
			echo "<script>
				alert('ERROR : Please Check Tax Dates.');
			</script>";
			exit();
		}
		
		$folder_name="tax_one";
	}
	else
	{
		$type='TAX_LIFETIME';
		$folder_name="tax_lifetime";
		$file_col="tax_lifetime";
		$date_from="";
		$date_to="";
	}
	
	$file="tax_copy";
	$date_from_col="tax_start";
	$date_to_col="tax_end";
}
else if($type=='FITNESS')
{
	$file="fitness_copy";
	$file_col="fitness";
	$date_from_col="fitness_start";
	$date_to_col="fitness_end";
	$date_from=$_POST['fitness_from'];
	$date_to=$_POST['fitness_to'];
	$folder_name="fitness";
}
else if($type=='P_5')
{
	$file="permit_five_copy";
	$file_col="permit_five";
	$date_from_col="permit_five_start";
	$date_to_col="permit_five_end";
	$date_from=$_POST['permit_5_from'];
	$date_to=$_POST['permit_5_to'];
	$folder_name="permit_five";
}
else if($type=='P_1')
{
	$file="permit_one_copy";
	$file_col="permit_one";
	$date_from_col="permit_one_start";
	$date_to_col="permit_one_end";
	$date_from=$_POST['permit_1_from'];
	$date_to=$_POST['permit_1_to'];
	$folder_name="permit_one";
}
else if($type=='PUC')
{
	$file="puc_copy";
	$file_col="puc";
	$date_from_col="puc_start";
	$date_to_col="puc_end";
	$date_from=$_POST['puc_from'];
	$date_to=$_POST['puc_to'];
	$folder_name="puc";
}
else if($type=='RC_FRONT')
{
	$file="rc_front_copy";
	$file_col="rc_front";
	$date_from_col="";
	$date_to_col="";
	$date_from="";
	$date_to="";
	$folder_name="rc_front";
}
else if($type=='RC_REAR')
{
	$file="rc_rear_copy";
	$file_col="rc_rear";
	$date_from_col="";
	$date_to_col="";
	$date_from="";
	$date_to="";
	$folder_name="rc_rear";
}
else
{
	echo "<script>
		alert('Invalid option selected : $type.');
		window.location.href='./upload.php';
	</script>";
	exit();
}

if($file=="")
{
	echo "<script>
			alert('ERROR : File not found.');
		</script>";
	exit();
}

$file_array=array("RC_REAR","RC_FRONT","TAX_LIFETIME");
	
if(!in_array($type,$file_array) AND ($date_from=="" || $date_to==""))
{
	echo "<script>
			alert('ERROR : PLEASE CHECK DATES.');
		</script>";
	exit();
}

$sourcePath = $_FILES[$file]['tmp_name'];
$targetPath=$folder_name."/".$fix_name.".".pathinfo($_FILES[$file]["name"],PATHINFO_EXTENSION);	

$maxDimW = "1200";
$maxDimH = "1200";

$file_type=$type;

$qry1=mysqli_query($conn,"SELECT `$file_col` as file1 FROM own_truck_docs WHERE tno='$tno'");
$row=mysqli_fetch_array($qry1);
if($row['file1']!='')
{
	unlink($row['file1']);	
}

include ('./up_func.php');

	if(!move_uploaded_file($sourcePath,$targetPath))
	{
		echo "
		<script type='text/javascript'> 
			alert('ERROR While uploading PUC.');
		</script>";
		exit();
	}

if($file_type=='RC_REAR' || $file_type=='RC_FRONT' || $file_type=='TAX_LIFETIME')
{
	$update=mysqli_query($conn,"UPDATE own_truck_docs SET `$file_col`='$targetPath' WHERE tno='$tno'");
	if(!$update)
	{
		echo mysqli_error($conn);exit();
	}
}	
else
{
	$update=mysqli_query($conn,"UPDATE own_truck_docs SET `$file_col`='$targetPath' WHERE tno='$tno'");
	if(!$update)
	{
		echo mysqli_error($conn);exit();
	}
	
	$update_dates=mysqli_query($conn,"UPDATE own_truck_docs_exp SET `$date_from_col`='$date_from',`$date_to_col`='$date_to' 
	WHERE tno='$tno'");
	
	if(!$update_dates)
	{
		echo mysqli_error($conn);exit();
	}
}

echo "<script>
	alert('SUCCESS : Files successfully uploaded.');
	$('#get_button').click();
</script>";
?>