<?php
require_once 'connect.php';
$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RRPL</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 1;">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<div id="result2"></div>


<script>
    $(function() {
    $( "#tno" ).autocomplete({
      source: '../diary/autofill/own_tno.php',
	  change: function (event, ui) {
        if(!ui.item){
            $(event.target).val("");
			alert('Truck No does not exists.');
			$("#tno").val("");
			$("#tno").focus();
		}
    }, 
    focus: function (event, ui){
        return false;
    }
    });
  });
</script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

				<script type="text/javascript">
								function fetch(tno){
                                            $("#loadicon").show();
                                            jQuery.ajax({
                                                url: "./fetch_truck_manage.php",
                                                data: 'tno=' + tno,
                                                type: "POST",
                                                success: function(data){
                                                    $("#result").html(data);
                                                    $("#loadicon").hide();
                                                },
                                                error: function() {}
                                 });
                                 }
				</script>

<body>
<a href="./"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:">Dashboard</button></a>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-md-12">			
	
	<div class="col-md-4 col-md-offset-4">
		<div class="row">
			<div class="col-md-8">	
				<label>Enter Truck No. <font color="red"><sup>*</sup></font></label>
				<input type="text" id="tno" style="text-transform:uppercase" class="form-control" required />
			</div>
			
			<div class="col-md-4">	
				<label>&nbsp;</label>
				<br>
				<button type="button" id="get_button" onclick="fetch($('#tno').val())" class="btn btn-danger">Get Details</button>
			</div>
		</div>
	</div>

	<div id="result" class="col-md-8 col-md-offset-2">
	
	</div>
	
</div>
</body>
</html>