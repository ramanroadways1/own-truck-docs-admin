<?php
require_once 'connect.php';
$today = date('Y-m-d');
?>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RRPL</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="tphead.js" type="text/javascript"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
	
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity:0.7;">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<script>
<!--
//Disable right mouse click Script
var message="Function Disabled!";
///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}
function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}
document.oncontextmenu=new Function("return false")

function disableCtrlKeyCombination(e)
{
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('a', 'c', 'x', 'v');
        var key;
        var isCtrl;
        if(window.event)
        {
                key = window.event.keyCode;     //IE
                if(window.event.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        else
        {
                key = e.which;     //firefox
                if(e.ctrlKey)
                        isCtrl = true;
                else
                        isCtrl = false;
        }
        //if ctrl is pressed check if other key is in forbidenKeys array
        if(isCtrl)
        {
                for(i=0; i<forbiddenKeys.length; i++)
                {
                        //case-insensitive comparation
                        if(forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase())
                        {
                                alert('Key combination CTRL + ' +String.fromCharCode(key)+' has been disabled.');
                                return false;
                        }
                }
        }
        return true;
}
// --> 
</script>

<div id="result2"></div>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<script type="text/javascript">
$(document).ready(function (e) {
$("#TruckForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#add_button").attr("disabled", true);
$("#add_button").html("Loading...");
	$.ajax({
	url: "./save_add_new_truck.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#result1").html(data);
		$("#add_button").attr("disabled", false);
		$("#add_button").html("ADD NEW VEHICLE");
		$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script> 

<div id="result1"></div>

<body onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<a href="./"><button class="btn btn-primary" style="margin-top:10px;margin-left:10px;letter-spacing:">Dashboard</button></a>

<div class="container" style="font-family:Verdana">	
<br />
<br />

<form id="TruckForm" autocomplete="off">

	<div class="form-group col-md-3">
		<label>Truck Number <font color="red">*</font></label>
		<input type="text" class="form-control" name="tno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Truck Type <font color="red">*</font></label>
		<select name="truck_type" class="form-control" required>
			<option value="">--SELECT Type--</option>
			<option value="TRUCK">TRUCK</option>
			<option value="TRAILER">TRAILER</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>Company <font color="red">*</font></label>
		<select name="company" class="form-control" required>
			<option value="">--SELECT COMPANY--</option>
			<option value="RRPL">RRPL</option>
			<option value="RAMAN_ROADWAYS">RAMAN_ROADWAYS</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>Wheeler <font color="red">*</font></label>
		<select name="wheeler" class="form-control" required>
			<option value="">--SELECT WHEELER--</option>
			<option value="04">04</option>
			<option value="06">06</option>
			<option value="10">10</option>
			<option value="12">12</option>
			<option value="14">14</option>
			<option value="16">16</option>
			<option value="18">18</option>
			<option value="22">22</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>Model <font color="red">*</font></label>
		<select data-size="8" name="model" class="form-control selectpicker" data-live-search="true" required>
			<option data-tokens="" value="">--SELECT MODEL--</option>
			<?php
			$qry_model = Qry($conn,"SELECT model FROM dairy.model_list ORDER BY model ASC");
		
			if(numRows($qry_model)>0)
			{
				while($row_m = fetchArray($qry_model))
				{
					echo "<option data-tokens='$row_m[model]' value='$row_m[model]'>$row_m[model]</option>";
				}
			}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>Fastag A/c No <font color="red">*</font></label>
		<input type="text" class="form-control" name="fastag_acno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Fastag Sr_No <font color="red">*</font></label>
		<input type="text" class="form-control" name="fastag_srno" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Supervisor Name <font color="red">*</font></label>
		<select name="supervisor" class="form-control" required>
			<option value="">--SELECT Supervisor--</option>
			<?php
			$fetch_supervisor = Qry($conn,"SELECT id,title FROM dairy.user WHERE username!='NA' AND role='1' AND type='1' ORDER BY username ASC");
			
			if(numRows($fetch_supervisor)>0)
			{
				while($row_s=fetchArray($fetch_supervisor))
				{
					echo "<option value='$row_s[id]'>$row_s[title]</option>";
				}	
			}
			?>
		</select>
	</div>
	
	<div class="form-group col-md-12">
		<h4 class="bg-primary col-md-12" style="padding:4px;">
			Body Measurement : 
		</h4>
		
		<div class="row">	
			<div class="form-group col-md-3">
				<label>Height (in inch)<font color="red">*</font></label>
				<input type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="body_height" required>
			</div>
			
			<div class="form-group col-md-3">
				<label>Width (in inch)<font color="red">*</font></label>
				<input type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="body_width" required>
			</div>
			
			<div class="form-group col-md-3">
				<label>Length (in inch)<font color="red">*</font></label>
				<input type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="body_length" required>
			</div>
			
			<div class="form-group col-md-3">
				<label>Floor to Top (in inch)<font color="red">*</font></label>
				<input type="number" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="body_floor_to_top" required>
			</div>
		</div>
	</div>
	
	<div class="form-group col-md-3">
		<label>Body Type <font color="red">*</font></label>
		<select name="body_type" class="form-control" required>
			<option value="">--SELECT BODY_TYPE--</option>
			<option value="FULL_OPEN">FULL/OPEN BODY</option>
			<option value="CLOSE">CLOSE BODY</option>
			<option value="CHILD">BACHCHA/CHILD BODY</option>
			<option value="PLATEFORM">PLATEFORM BODY</option>
			<option value="BULKER">BULKER</option>
			<option value="TANKER">TANKER</option>
			<option value="CONTAINER">CONTAINER BODY</option>
			<option value="TIPPER">TIPPER BODY</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>How Much Axle <font color="red">*</font></label>
		<select name="axle_count" class="form-control" required>
			<option value="">--SELECT AXLE--</option>
			<option value="DOUBLE">DOUBLE AXLE</option>
			<option value="TRIPLE">TRIPLE AXLE</option>
			<option value="SIX">SIX AXLE</option>
		</select>
	</div>
	
	<div class="form-group col-md-3">
		<label>Axle Company <font color="red">*</font></label>
		<input type="text" class="form-control" name="axle_company" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Axle Wheel Bolt QTY <font color="red">*</font></label>
		<input type="text" class="form-control" name="axle_bolt_qty" oninput="this.value=this.value.replace(/[^0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Chasis Number <font color="red">*</font></label>
		<input type="text" class="form-control" name="chasis_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Chasis Number <font color="red">(image) *</font></label>
		<input type="file" class="form-control" name="chasis_img" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Engine Number <font color="red">*</font></label>
		<input type="text" class="form-control" name="engine_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Engine Number <font color="red">(image) *</font></label>
		<input type="file" class="form-control" name="engine_img" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Gear Number <font color="red">*</font></label>
		<input type="text" class="form-control" name="gear_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Gear Number <font color="red">(image) *</font></label>
		<input type="file" class="form-control" name="gear_img" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Cabin Number <font color="red">*</font></label>
		<input type="text" class="form-control" name="cabin_no" oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Cabin Number <font color="red">(image) *</font></label>
		<input type="file" class="form-control" name="cabin_img" required>
	</div>

	<div class="form-group col-md-3">
		<label>Bought On <font color="red">*</font></label>
		<input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" name="bought_on" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>On Road On <font color="red">*</font></label>
		<input type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" name="on_road_on" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Empty Weight <font color="red">*</font></label>
		<input type="number" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/,'')" name="empty_weight" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Pictures <font color="red">(with no. plate) *</font></label>
		<input type="file" class="form-control" multiple="multiple" name="vehicle_pic[]" required>
	</div>
	
	<div class="form-group col-md-3">
		<label>Bought From <font color="red">*</font></label>
		<textarea class="form-control" name="bought_from" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" required></textarea>
	</div>
	
	<div class="form-group col-md-3">
		<label>Body Created By <font color="red">*</font></label>
		<textarea class="form-control" name="body_created_by" oninput="this.value=this.value.replace(/[^a-z A-Z0-9,]/,'')" required></textarea>
	</div>
	
	<div class="form-group col-md-12">
		<button type="submit" id="add_button" class="pull-right btn-lg btn btn-danger">ADD NEW VEHICLE</button>
	</div>
	
</form>

</div>
	<br />
	<br />
	<br />
</body>
</html>