<?php 
list($width, $height, $type, $attr) = getimagesize($sourcePath_Chasis);
if ( $width > $maxDimW_Chasis || $height > $maxDimH_Chasis ) {
    $target_filename = $sourcePath_Chasis;
    $fn = $sourcePath_Chasis;
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDimW_Chasis;
        $height = $maxDimH_Chasis/$ratio;
    } else {
        $width = $maxDimW_Chasis*$ratio;
        $height = $maxDimH_Chasis;
    }
    $src = @imagecreatefromstring(@file_get_contents(@$fn));
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );

    imagejpeg($dst, $target_filename); // adjust format as needed
}
?>