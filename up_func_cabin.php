<?php 
list($width, $height, $type, $attr) = getimagesize($sourcePath_Cabin);
if ( $width > $maxDimW_Cabin || $height > $maxDimH_Cabin ) {
    $target_filename = $sourcePath_Cabin;
    $fn = $sourcePath_Cabin;
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDimW_Cabin;
        $height = $maxDimH_Cabin/$ratio;
    } else {
        $width = $maxDimW_Cabin*$ratio;
        $height = $maxDimH_Cabin;
    }
    $src = @imagecreatefromstring(@file_get_contents(@$fn));
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );

    imagejpeg($dst, $target_filename); // adjust format as needed
}
?>