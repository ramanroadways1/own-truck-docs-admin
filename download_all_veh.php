<?php 
require_once './connect.php';

$output = '';

$query = "SELECT * FROM own_truck_docs_exp ORDER BY id ASC";

$result = mysqli_query($conn,$query);

if(!$result)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($result) > 0)
 {
	 
 $output .= '
   <table border="1">  
                    <tr>  
                        <th>Truck No</th>
						<th>Permit(1Yr)<br>Start</th>
						<th>Permit(1Yr)<br>Ends</th>
						<th>Permit(5Yr)<br>Start</th>
						<th>Permit(5Yr)<br>Ends</th>
						<th>Fitness<br>Start</th>
						<th>Fitness<br>Ends</th>
						<th>Tax<br>Start</th>
						<th>Tax<br>Ends</th>
						<th>Insurance<br>Start</th>
						<th>Insurance<br>Ends</th>
						<th>PUC<br>Start</th>
						<th>PUC<br>Ends</th>
					</tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
	  if($row['permit_one_start']!=0)
				{ $p1_start=date("d/m/y",strtotime($row["permit_one_start"])); }
				else { $p1_start="NULL"; }
				
				if($row['permit_one_end']!=0)
				{ $p1_end=date("d/m/y",strtotime($row["permit_one_end"])); }
				else { $p1_end="NULL"; }
				
				if($row['permit_five_start']!=0)
				{ $p5_start=date("d/m/y",strtotime($row["permit_five_start"])); }
				else { $p5_start="NULL"; }
				
				if($row['permit_five_end']!=0)
				{ $p5_end=date("d/m/y",strtotime($row["permit_five_end"])); }
				else { $p5_end="NULL"; }
				
				if($row['fitness_start']!=0)
				{ $fitness_start=date("d/m/y",strtotime($row["fitness_start"])); }
				else { $fitness_start="NULL"; }
				
				if($row['fitness_end']!=0)
				{ $fitness_end=date("d/m/y",strtotime($row["fitness_end"])); }
				else { $fitness_end="NULL"; }
				
				if($row['tax_start']!=0)
				{ $tax_start=date("d/m/y",strtotime($row["tax_start"])); }
				else { $tax_start="NULL"; }
				
				if($row['tax_end']!=0)
				{ $tax_end=date("d/m/y",strtotime($row["tax_end"])); }
				else { $tax_end="NULL"; }
				
				if($row['ins_start']!=0)
				{ $ins_start=date("d/m/y",strtotime($row["ins_start"])); }
				else { $ins_start="NULL"; }
				
				if($row['ins_end']!=0)
				{ $ins_end=date("d/m/y",strtotime($row["ins_end"])); }
				else { $ins_end="NULL"; }
				
				if($row['puc_start']!=0)
				{ $puc_start=date("d/m/y",strtotime($row["puc_start"])); }
				else { $puc_start="NULL"; }
				
				if($row['puc_end']!=0)
				{ $puc_end=date("d/m/y",strtotime($row["puc_end"])); }
				else { $puc_end="NULL"; }
				
				
   $output .= '
    <tr>  
		<td>'.$row["tno"].'</td>  
		<td>'.$p1_start.'</td>  
		<td>'.$p1_end.'</td>  
		<td>'.$p5_start.'</td>  
		<td>'.$p5_end.'</td>  
		<td>'.$fitness_start.'</td>  
		<td>'.$fitness_end.'</td>  
		<td>'.$tax_start.'</td>  
		<td>'.$tax_end.'</td>  
		<td>'.$ins_start.'</td>  
		<td>'.$ins_end.'</td>  
		<td>'.$puc_start.'</td>  
		<td>'.$puc_end.'</td>  
	</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=ALL_VEHICLES_LIST.xls');
  echo $output;
 }
 else
 {
	 echo "<script>
			alert('No result found..');
			window.location.href='./list_all_veh.php';
		</script>";
 }

mysqli_close($conn);
?>