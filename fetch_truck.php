<?php
require_once("./connect.php");
$today=date('Y-m-d');

$tno = escapeString($conn,strtoupper($_POST['tno']));

if($tno!='')
{
?>
<div class="form-group col-md-12">	
	<div class="row">
		<b>Showing result of : <font color="maroon"><?php echo $tno; ?></font></b>	
	</div>
</div>

<?php	
$qry = Qry($conn,"SELECT own_truck_docs.*,own_truck_docs_exp.* FROM own_truck_docs,own_truck_docs_exp WHERE 
own_truck_docs.tno='$tno' AND own_truck_docs.tno=own_truck_docs_exp.tno");

if(numRows($qry)==0)
{
	echo "<script>
		alert('Invalid Truck no entered.');
		window.location.href='./upload.php';
	</script>";
	exit();
}
	
$row = fetchArray($qry);

if($row['rc_front']=='' || $row['rc_front']=='NA')
{
?>
<script type="text/javascript">
$(document).ready(function (e) {
$("#SaveForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
 $("#submit_docs").attr("disabled",true);
 $.ajax({
	url: "./docs_submit.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data){
		$("#result2").html(data);
	},
	error: function() 
	{} });}));});
</script>

<form id="SaveForm">

<div class="form-group col-md-12">&nbsp;</div>
	
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>RC Front & Rear</b></center></div>	
		
	<div class="form-group col-md-12">
		<br>
		<label>Upload RC (front side) <font color='red'><sup>*</sup></font></label>
		<input type='file' name='rc_front' class='form-control' required />
	</div>
	<div class="form-group col-md-12">
		<label>Upload RC (rear side) <font color='red'><sup>*</sup></font></label>
		<input type='file' name='rc_rear' class='form-control' required />
	</div>
</div>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>PUC</b></center></div>	

	<div class="form-group col-md-12">
		<br>			
		<label>Upload PUC <sup style="color:red">(optional)</sup></label>
		<input type='file' name='puc' class='form-control' />
	</div>
		
	<div class="form-group col-md-6">
			<label>PUC (valid from) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" name="puc_from" class="form-control" />
	</div>
		
	<div class="form-group col-md-6">
			<label>PUC (valid to) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" name="puc_to" class="form-control" />
	</div>
		
</div>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Permit (1 Yr)</b></center></div>		
		
	<div class="form-group col-md-12">
		<br>	
		<label>Upload Permit <sup style="color:red">(optional)</sup></label>
		<input type='file' name='permit_copy_1' class='form-control' />
	</div>	
		
	<div class="form-group col-md-6">	
			<label>Permit 1yr (valid from) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" name="permit_1_from" class="form-control" />
	</div>	
		
	<div class="form-group col-md-6">
			<label>Permit 1yr (valid to) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" name="permit_1_to" class="form-control" />
	</div>
		
</div>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Permit (5 Yr)</b></center></div>			
	
	<div class="form-group col-md-12">
		<br>
		<label>Upload Permit <font color='red'><sup>5 year *</sup></font></label>
		<input type='file' name='permit_copy_5' id="permit_copy_5" class='form-control' required />
	</div>	
		
	<div class="form-group col-md-6">	
			<label>Permit 5yr (valid from) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="permit_5_from" type="date" name="permit_5_from" class="form-control" required />
	</div>	
		
	<div class="form-group col-md-6">	
		<label>Permit 5yr (valid to) </label>	
		<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="permit_5_to" type="date" name="permit_5_to" class="form-control" required />
	</div>
	
	<script>
	function P5Sel(elem)
	{
		$('#permit_copy_5').val('');
		$('#permit_5_from').val('');
		$('#permit_5_to').val('');
			
		if(elem=='NA')
		{
			$('#permit_copy_5').attr('disabled',true);
			$('#permit_5_from').attr('readonly',true);
			$('#permit_5_to').attr('readonly',true);
		}
		else
		{
			$('#permit_copy_5').attr('disabled',false);
			$('#permit_5_from').attr('readonly',false);
			$('#permit_5_to').attr('readonly',false);
		}
	}
	</script>
	
	<div class="form-group col-md-12">	
		<label>Permit-5Yr NA </label>
		<select onchange="P5Sel(this.value)" class="form-control" name="p_5_na">
			<option value="">Yes - Available</option>
			<option value="NA">Not Available</option>
		</select>		
	</div>
		
</div>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Fitness</b></center></div>
	
	<div class="form-group col-md-12">
		<br>
		<label>Upload Fitness <font color='red'><sup>*</sup></label>
		<input type='file' name='fitness' id="fitness" class='form-control' required />
	</div>
		
	<div class="form-group col-md-6">	
			<label>Fitness (valid from) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="fitness_from" type="date" name="fitness_from" class="form-control" required  />
	</div>
		
	<div class="form-group col-md-6">	
			<label>Fitness (valid to) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="fitness_to" type="date" name="fitness_to" class="form-control" required  />
	</div>
	
	<script>
	function FitnessSel(elem)
	{
		$('#fitness').val('');
		$('#fitness_from').val('');
		$('#fitness_to').val('');
			
		if(elem=='NA')
		{
			$('#fitness').attr('disabled',true);
			$('#fitness_from').attr('readonly',true);
			$('#fitness_to').attr('readonly',true);
		}
		else
		{
			$('#fitness').attr('disabled',false);
			$('#fitness_from').attr('readonly',false);
			$('#fitness_to').attr('readonly',false);
		}
	}
	</script>
	
	<div class="form-group col-md-12">	
		<label>Fitness NA </label>
		<select onchange="FitnessSel(this.value)" class="form-control" name="fitness_na">
			<option value="">Yes - Available</option>
			<option value="NA">Not Available</option>
		</select>		
	</div>
	
</div>	
		
	
	<script>
		function TaxType(elem)
		{
			if(elem=='1')
			{
				$('#tax1').hide();
				$('#tax2').hide();
				$('#tax_from').attr('required',false);
				$('#tax_to').attr('required',false);
			}
			else
			{
				$('#tax1').show();	
				$('#tax2').show();	
				$('#tax_from').attr('required',true);
				$('#tax_to').attr('required',true);
			}
		}
		</script>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>TAX Receipt</b></center></div>
			
		<div class="form-group col-md-4">	
			<br>
			<label>Tax Type <font color='red'><sup>*</sup></label>
			<select class='form-control' onchange='TaxType(this.value)' id="tax_type" style='border:1px solid #000' name='tax_type' required='required'>
				<option value=''>Tax Type</option>
				<option value='0'>Normal</option>
				<option value='1'>Lifetime</option>
			</select>
		</div>	
		
		<div class="form-group col-md-8">
			<br>
			<label>Upload TAX <font color='red'><sup>*</sup></label>
			<input type='file' name='tax' id="tax_file" class='form-control' required />
		</div>
		
		<div class="form-group col-md-6" id="tax1">
			<label>TAX (valid from) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="tax_from" type="date" name="tax_from" class="form-control" required  />
		</div>
		
		<div class="form-group col-md-6" id="tax2">
			<label>TAX (valid to) </label>
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="tax_to" type="date" name="tax_to" class="form-control" required  />
		</div>
		
	<script>
	function TaxSel(elem)
	{
		$('#tax_type').val('');
		$('#tax_file').val('');
		$('#tax_from').val('');
		$('#tax_to').val('');
			
		if(elem=='NA')
		{
			$('#tax_type').attr('disabled',true);
			$('#tax_file').attr('disabled',true);
			$('#tax_from').attr('readonly',true);
			$('#tax_to').attr('readonly',true);
		}
		else
		{
			$('#tax_type').attr('disabled',false);
			$('#tax_file').attr('disabled',false);
			$('#tax_from').attr('readonly',false);
			$('#tax_to').attr('readonly',false);
		}
	}
	</script>
	
	<div class="form-group col-md-12">	
		<label>Tax NA </label>
		<select onchange="TaxSel(this.value)" class="form-control" name="tax_na">
			<option value="">Yes - Available</option>
			<option value="NA">Not Available</option>
		</select>		
	</div>
		
</div>		
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Insurance</b></center></div>		
	
		<div class="form-group col-md-12">
			<br>
			<label>Upload Insurance <font color='red'><sup>*</sup></label>
			<input type='file' name='ins' id="ins_file" class='form-control' required />
		</div>
		
		<div class="form-group col-md-6">
			<label>Insurance (valid from) </label>
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" id="ins_from" name="ins_from" class="form-control" required  />
		</div>
		
		<div class="form-group col-md-6">
			<label>Insurance (valid to) </label>
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" type="date" id="ins_to" name="ins_to" class="form-control" required  />
		</div>
		
		<script>
	function InsSel(elem)
	{
		$('#ins_file').val('');
		$('#ins_from').val('');
		$('#ins_to').val('');
			
		if(elem=='NA')
		{
			$('#ins_file').attr('disabled',true);
			$('#ins_from').attr('readonly',true);
			$('#ins_to').attr('readonly',true);
		}
		else
		{
			$('#ins_file').attr('disabled',false);
			$('#ins_from').attr('readonly',false);
			$('#ins_to').attr('readonly',false);
		}
	}
	</script>
	
	<div class="form-group col-md-12">	
		<label>Insrurance NA </label>
		<select onchange="InsSel(this.value)" class="form-control" name="ins_na">
			<option value="">Yes - Available</option>
			<option value="NA">Not Available</option>
		</select>		
	</div>
		
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
	
</div>	

<div class="form-group col-md-12">
	<input type='submit' style="width:30%" id='submit_docs' class='btn btn-danger btn-lg btn-block' name='submit' value="Upload Docs" />
</div>
	
</form>	
<?php
}
else
{
	if($row['tax']!='')
	{
		$tax_type="0";
	}
	else
	{
		$tax_type="1";
	}
	?>
	
	<div id="result_qry"></div>

	<div class="form-group col-md-4" style="border:1px solid #DDD">
	<form id="RcFrontForm" action="" method="POST">
		
		<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>RC (front)</b></center></div>

		<div class="form-group col-md-12">
			<br>
			<label>Upload RC (front)</label>
			<input type="file" name="rc_front_copy" id="rc_front_copy" class='form-control' required />
		</div>
		<input type="hidden" name="type" value="RC_FRONT">
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<div class="form-group col-md-12">
			<button type="submit" id="button_rc_front" class="btn btn-warning">Change RC (front)</button>
			<?php
			if($row['rc_front']!='' AND $row['rc_front']!='NA')
			{
			?>
				<a href="<?php echo $row['rc_front'] ?>" target="_blank"><button type="button" class="btn btn-success">View Rc Front</button></a>	
			<?php			
			}
			else
			{
			?>
				<button onclick="alert('No attachment fond !')" type="button" class="btn btn-success">View Rc Front</button>
			<?php	
			}
			?>
			
		</div>		
	</form>
	</div>
			
	<div class="form-group col-md-4" style="border:1px solid #DDD">		
	<form id="RcRearForm">
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>RC (rear)</b></center></div>
		<div class="form-group col-md-12">
			<br>
			<label>Upload RC (rear)</label>
			<input type="file" name="rc_rear_copy" id="rc_rear_copy" class='form-control' required />
		</div>
		<input type="hidden" name="type" value="RC_REAR">
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<div class="form-group col-md-12">
			<button type="submit" id="button_rc_rear" class="btn btn-warning">Change RC (rear)</button>
			
			<?php
			if($row['rc_rear']!='' AND $row['rc_rear']!='NA')
			{
			?>
				<a href="<?php echo $row['rc_rear'] ?>" target="_blank"><button type="button" class="btn btn-success">View Rc Rear</button></a>	
			<?php			
			}
			else
			{
			?>
				<button onclick="alert('No attachment fond !')" type="button" class="btn btn-success">View Rc Rear</button>
			<?php	
			}
			?>
		</div>	
	</form>
	</div>
	
	<div class="form-group col-md-4" style="border:1px solid #DDD">			
	<form id="PUCForm">
		<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>PUC</b></center></div>
		
		<div class="form-group col-md-12">
			<br>
			<label>Upload PUC</label>
			<input type="file" name="puc_copy" id="puc_copy" class='form-control' required />
		</div>	
		<div class="form-group col-md-6">
			<label>PUC (valid from) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required type="date" value="<?php echo $row['puc_start']; ?>" name="puc_from" class="form-control" />
		</div>	
		<div class="form-group col-md-6">
			<label>PUC (valid to) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required value="<?php echo $row['puc_end']; ?>" type="date" name="puc_to" class="form-control" />
		</div>
			<input type="hidden" name="tno" value="<?php echo $tno; ?>">
			<input type="hidden" name="type" value="PUC">
			
		<div class="form-group col-md-12">
			<button type="submit" id="button_puc" class="btn btn-warning">Change PUC</button>	
			<a href="<?php echo $row['puc'] ?>" target="_blank">
				<button <?php if($row['puc']=='' || $row['puc']=='NA') {echo "disabled"; } ?> type="button" class="btn btn-success">View PUC</button>
			</a>
		</div>
	</form>	
</div>
	
	<div class="form-group col-md-4" style="border:1px solid #DDD">		
			<form id="Permit1Form">
		<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Permit (1 Yr)</b></center></div>	
			
		<div class="form-group col-md-12">
			<br>
			<label>Upload Permit <font color='red'><sup>1 year *</sup></font></label>
			<input type="file" name="permit_one_copy" id="permit_one_copy" class='form-control' required />
		</div>	
		
		<div class="form-group col-md-6">
			<label>Permit 1yr (valid from) </label>		
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['permit_one_start']; ?>" required type="date" name="permit_1_from" class="form-control" />
		</div>
		
		<div class="form-group col-md-6">
			<label>Permit 1yr (valid to) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['permit_one_end']; ?>" required type="date" name="permit_1_to" class="form-control" />
		</div>
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<input type="hidden" name="type" value="P_1">
		<div class="form-group col-md-12">
			<button type="submit" id="button_permit_one" class="btn btn-warning">Change Permit</button>
			<a href="<?php echo $row['permit_one'] ?>" target="_blank">
				<button <?php if($row['permit_one']=='' || $row['permit_one']=='NA') {echo "disabled"; } ?> type="button" class="btn btn-success">View Permit (1 Yr)</button>
			</a>
		</div>
	</form>
	</div>	
	
	
<div class="form-group col-md-4" style="border:1px solid #DDD">		
	<form id="Permit5Form">
		<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Permit (5 Yr)</b></center></div>		
		
		<div class="form-group col-md-12">
			<br>
			<label>Upload Permit <font color='red'><sup>5 year *</sup></font></label>
			<input type="file" name="permit_five_copy" id="permit_five_copy" class='form-control' required />
		</div>	
		
		<div class="form-group col-md-6">
			<label>Permit 5yr (valid from) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['permit_five_start']; ?>" type="date" name="permit_5_from" class="form-control" required />
		</div>
		
		<div class="form-group col-md-6">
			<label>Permit 5yr (valid to) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['permit_five_end']; ?>" type="date" name="permit_5_to" class="form-control" required />
		</div>
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<input type="hidden" name="type" value="P_5">
		<div class="form-group col-md-12">
			<button type="submit" id="button_permit_five" class="btn btn-warning">Change Permit</button>
			<a href="<?php echo $row['permit_five'] ?>" type="button" target="_blank">
				<button <?php if($row['permit_five']=='' || $row['permit_five']=='NA') {echo "disabled"; } ?> class="btn btn-success" type="button">View Permit (5 Yr)</button>
			</a>
		</div>	
	</form>
</div>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">		
			<form id="FitnessForm">	
		<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Fitness</b></center></div>		
		
		<div class="form-group col-md-12">
			<br>
			<label>Upload Fitness <font color='red'><sup>*</sup></label>
			<input type="file" name="fitness_copy" id="fitness_copy" class='form-control' required />
		</div>	
		
		<div class="form-group col-md-6">
			<label>Fitness (valid from) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['fitness_start']; ?>" type="date" name="fitness_from" class="form-control" required  />
		</div>
		
		<div class="form-group col-md-6">
			<label>Fitness (valid to) </label>	
			<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['fitness_end']; ?>" type="date" name="fitness_to" class="form-control" required  />
		</div>
		
		<input type="hidden" name="tno" value="<?php echo $tno; ?>">
		<input type="hidden" name="type" value="FITNESS">
		
		<div class="form-group col-md-12">
			<button type="submit" id="button_fitness" class="btn btn-warning">Change Fitness</button>
			<a href="<?php echo $row['fitness'] ?>" target="_blank">
				<button <?php if($row['fitness']=='' || $row['fitness']=='NA') {echo "disabled"; } ?> class="btn btn-success" type="button">View Fitness</button>
			</a>
		</div>
	</form>
</div>
		
		<script>
		function TaxType(elem)
		{
			if(elem==1)
			{
				$('#tax1').hide();
				$('#tax2').hide();
				$('#tax_from').attr('required',false);
				$('#tax_to').attr('required',false);
			}
			else
			{
				$('#tax1').show();	
				$('#tax2').show();	
				$('#tax_from').attr('required',true);
				$('#tax_to').attr('required',true);
			}
		}
		</script>
		
<div class="form-group col-md-4" style="border:1px solid #DDD">	
	<form id="TaxForm">
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>TAX Receipt</b></center></div>	
	
	<div class="form-group col-md-4">
		<br>
		<label>TAX Type <font color='red'><sup>*</sup></label>
		<select class='form-control' onchange='TaxType(this.value)' style='border:1px solid #000' id='tax_type1' name='tax_type1' required='required'>
				<option <?php if($tax_type=='0') { echo "selected"; } ?> value='0'>Normal</option>
				<option <?php if($tax_type=='1') { echo "selected"; } ?> value='1'>Lifetime</option>
		</select>
	</div>
	
	<div class="form-group col-md-8">	
		<br>
		<label>Upload Tax Receipt <font color='red'><sup>*</sup></label>
		<input type="file" name="tax_copy" id="tax_copy" class='form-control' required />
	</div>	
		
	<div class="form-group col-md-6" id="tax1">
		<label>TAX (valid from) </label>	
		<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['tax_start']; ?>" id="tax_from" type="date" name="tax_from" class="form-control" required  />
	</div>
	
	<div class="form-group col-md-6" id="tax2">	
		<label>TAX (valid to) </label>
		<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['tax_end']; ?>" id="tax_to" type="date" name="tax_to" class="form-control" required  />
	</div>
	<input type="hidden" name="tno" value="<?php echo $tno; ?>">
	<input type="hidden" name="type" value="TAX">
	
	<div class="form-group col-md-12">
		<button type="submit" id="button_tax" class="btn btn-warning">Change TAX</button>
		<a href="<?php if($tax_type=='0') { echo $row['tax']; } else { echo $row['tax_lifetime']; } ?>" target="_blank">
			<button <?php if(($row['tax']=='' || $row['tax']=='NA') AND ($row['tax_lifetime']=='' || $row['tax_lifetime']=='NA')) {echo "disabled"; } ?> class="btn btn-success" type="button">View TAX Receipt</button>
		</a>
	</div>
		
	</form>
</div>

<div class="form-group col-md-4" style="border:1px solid #DDD">
	<form id="InsForm">
	<div class="bg-primary row" style="padding:4px;font-size:13px"><center><b>Insurance</b></center></div>
	
	<div class="form-group col-md-12">
		<br>
		<label>Upload Insurance <font color='red'><sup>*</sup></label>
		<input type="file" name="ins_copy" id="ins_copy" class='form-control' required />
	</div>
	
	<div class="form-group col-md-6">
		<label>Insurance (valid from) </label>
		<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['ins_start']; ?>" type="date" name="ins_from" class="form-control" required  />
	</div>
	
	<div class="form-group col-md-6">
		<label>Insurance (valid to) </label>
		<input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" value="<?php echo $row['ins_end']; ?>" type="date" name="ins_to" class="form-control" required  />
	</div>
	
	<input type="hidden" name="tno" value="<?php echo $tno; ?>">
	<input type="hidden" name="type" value="INS">
		
	<div class="form-group col-md-12">
		<button type="submit" id="button_ins" class="btn btn-warning">Change Insurance</button>
		<a href="<?php echo $row['ins'] ?>" target="_blank">
			<button <?php if($row['ins']=='' || $row['ins']=='NA') {echo "disabled"; } ?> class="btn btn-success" type="button">View Insurance</button>
		</a>
	</div>
	</form>
</div>
	
<script type="text/javascript">
$(document).ready(function (e) {
$("#RcFrontForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_rc_front").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_rc_front").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#RcRearForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_rc_rear").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_rc_rear").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#PUCForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_puc").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_puc").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#Permit1Form").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_permit_one").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_permit_one").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#Permit5Form").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_permit_five").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_permit_five").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#FitnessForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_fitness").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_fitness").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>


<script type="text/javascript">
$(document).ready(function (e) {
$("#TaxForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_tax").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_tax").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
$(document).ready(function (e) {
$("#InsForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
$("#button_ins").attr("disabled",true);
 $.ajax({
	url: "./update_docs.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_qry").html(data);
	$("#loadicon").hide();
	$("#button_ins").attr("disabled",false);
	},
	error: function() 
	{} });}));});
</script>
	
	<script>
		TaxType('<?php echo $tax_type; ?>');
	</script>

<?php
}
}
?>