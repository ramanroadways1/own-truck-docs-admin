<?php
require_once 'connect.php';
$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RRPL</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 1;">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<div id="result2"></div>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body>
<a href="./"><button class="btn btn-danger" style="margin-top:10px;margin-left:10px;letter-spacing:">Dashboard</button></a>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-md-6 col-md-offset-3">			
	
	<table class="table table-bordered" style="font-family:Verdana;font-size:13px;">
		<tr>
			<th>Id</th>
			<th>Truck No</th>
		</tr>
		<?php
		$qry=mysqli_query($conn,"SELECT tno FROM own_truck_docs WHERE rc_front='' ORDER BY id ASC");
		if(mysqli_num_rows($qry)>0)
		{
			$i=1;
			while($row=mysqli_fetch_array($qry))
			{
				echo "<tr>
						<td>$i</td>
						<td>$row[tno]</td>
				</tr>";
			$i++;	
			}
		}
		else
		{
			echo "<tr>
					<td colspan='2'><b><font color='red'>No records found..</b></font></td>
			</tr>";
		}
		?>		
	</table>
	
</div>

</div>
</body>
</html>