<?php 
list($width, $height, $type, $attr) = getimagesize($sourcePath);
if ( $width > $maxDimW || $height > $maxDimH ) {
    $target_filename = $sourcePath;
    $fn = $sourcePath;
    $size = getimagesize( $fn );
    $ratio = $size[0]/$size[1]; // width/height
    if( $ratio > 1) {
        $width = $maxDimW;
        $height = $maxDimH/$ratio;
    } else {
        $width = $maxDimW*$ratio;
        $height = $maxDimH;
    }
    $src = @imagecreatefromstring(@file_get_contents(@$fn));
    $dst = imagecreatetruecolor( $width, $height );
    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );

    imagejpeg($dst, $target_filename); // adjust format as needed
}
?>