<?php
require_once 'connect.php';
$today=date('Y-m-d');
$days_ago=date('Y-m-d', strtotime('+20 days', strtotime($today)));
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RTO PORTAL || RAMAN ROADWAYS PRIVATE LTD.</title>
<meta http-equiv="refresh" content="240">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:12px;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12">
		<br>
				<h4 class="page-header" style="letter-spacing:1px;color:#000; font-size:25px; font-family: 'Baumans', cursive;">
				<center><b>Logged in as RTO ! </b></center></h4>
		</div>
	</div>

		<br />
	<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>Insurance</i></div>
<div class="panel-body" style="overflow-x:hidden;">
<?php
$ins=mysqli_query($conn,"SELECT tno,ins_start,ins_end,DATEDIFF(`ins_end`,'$today') as exp_days FROM own_truck_docs_exp WHERE 
ins_end<='$days_ago' AND ins_end!=0 ORDER BY ins_end ASC");

if(!$ins)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($ins)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i1=1;
		
	while($row1=mysqli_fetch_array($ins))
	{
	if($row1['exp_days']<=7)
	{
		$data='<td class="bg-danger">'.$row1['exp_days'].'</td>';
	}
    else if($row1['exp_days']<=20)
	{
		$data='<td class="bg-warning">'.$row1['exp_days'].'</td>';
	}
	else
	{
		$data='<td>'.$row1['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i1</td>
				<td>$row1[tno]</td>
				<td>".date('d/m/y',strtotime($row1['ins_start']))."</td>
				<td>".date('d/m/y',strtotime($row1['ins_end']))."</td>
				$data
			</tr>";
	$i1++;
	}	
	
echo "</table>";	
}
else
{
	echo "<br /><center><b>No Insurance going to expire in next 20 Days</b></center>";
}
?>
					</div>
					
					
				</div>
				
			</div><!--/.col-->
	
		<div class="col-md-6 col-sm-12">
			
				<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>Fitness</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$fitness=mysqli_query($conn,"SELECT tno,fitness_start,fitness_end,DATEDIFF(`fitness_end`,'$today') as exp_days FROM own_truck_docs_exp 
WHERE fitness_end<='$days_ago' AND fitness_end!=0 ORDER BY fitness_end ASC");

if(!$fitness)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($fitness)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i2=1;
		
	while($row2=mysqli_fetch_array($fitness))
	{
	if($row2['exp_days']<=7)
	{
		$data2='<td class="bg-danger">'.$row2['exp_days'].'</td>';
	}
    else if($row2['exp_days']<=20)
	{
		$data2='<td class="bg-warning">'.$row2['exp_days'].'</td>';
	}
	else
	{
		$data2='<td>'.$row2['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i2</td>
				<td>$row2[tno]</td>
				<td>".date('d/m/y',strtotime($row2['fitness_start']))."</td>
				<td>".date('d/m/y',strtotime($row2['fitness_end']))."</td>
				$data2
			</tr>";
		$i2++;		
	}	

echo "</table>";	
}
else
{
	echo "<br /><center><b>No Fitness going to expire in next 20 Days</b></center>";
}
?>
					</div>
				</div>
			</div><!--/.col-->
		</div>
		
		<br />

	<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>Permit (5 Yrs)</i></div>
<div class="panel-body" style="overflow-x:hidden;">
<?php
$permit_five=mysqli_query($conn,"SELECT tno,permit_five_start,permit_five_end,DATEDIFF(`permit_five_end`,'$today') as exp_days FROM 
own_truck_docs_exp WHERE permit_five_end<='$days_ago' AND permit_five_end!=0 order by permit_five_end asc");

if(!$permit_five)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($permit_five)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i3=1;
		
	while($row3=mysqli_fetch_array($permit_five))
	{
	if($row3['exp_days']<=7)
	{
		$data3='<td class="bg-danger">'.$row3['exp_days'].'</td>';
	}
    else if($row3['exp_days']<=20)
	{
		$data3='<td class="bg-warning">'.$row3['exp_days'].'</td>';
	}
	else
	{
		$data3='<td>'.$row3['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i3</td>
				<td>$row3[tno]</td>
				<td>".date('d/m/y',strtotime($row3['permit_five_start']))."</td>
				<td>".date('d/m/y',strtotime($row3['permit_five_end']))."</td>
				$data3
			</tr>";
		$i3++;	
	}	
	
echo "</table>";	
}
else
{
	echo "<br /><center><b>No Permit (5 Yrs) going to expire in next 20 Days</b></center>";
}
?>
					</div>
				</div>
			</div><!--/.col-->
	<div class="col-md-6 col-sm-12">
			
				<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>Permit (1 Yr)</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$permit_one=mysqli_query($conn,"SELECT tno,permit_one_start,permit_one_end,DATEDIFF(`permit_one_end`,'$today') as exp_days FROM 
own_truck_docs_exp WHERE permit_one_end<='$days_ago' AND permit_one_end!=0 order by permit_one_end asc");

if(!$permit_one)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($permit_one)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i4=1;
		
	while($row4=mysqli_fetch_array($permit_one))
	{
	if($row4['exp_days']<=7)
	{
		$data4='<td class="bg-danger">'.$row4['exp_days'].'</td>';
	}
    else if($row4['exp_days']<=20)
	{
		$data4='<td class="bg-warning">'.$row4['exp_days'].'</td>';
	}
	else
	{
		$data4='<td>'.$row4['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i4</td>
				<td>$row4[tno]</td>
				<td>".date('d/m/y',strtotime($row4['permit_one_start']))."</td>
				<td>".date('d/m/y',strtotime($row4['permit_one_end']))."</td>
				$data4
			</tr>";
		$i4++;	
	}	
	
echo "</table>";	
}
else
{
	echo "<br /><center><b>No Permit (1 Yr) going to expire in next 20 Days</b></center>";
}
?>
					</div>
				</div>
			</div><!--/.col-->
		</div>
		
		
				<br />
	<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>TAX Receipt</i></div>
<div class="panel-body" style="overflow-x:hidden;">
<?php
$tax=mysqli_query($conn,"SELECT tno,tax_start,tax_end,DATEDIFF(`tax_end`,'$today') as exp_days FROM 
own_truck_docs_exp WHERE tax_end<='$days_ago' AND tax_end!=0 order by tax_end asc");

if(!$tax)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($tax)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i5=1;
		
	while($row5=mysqli_fetch_array($tax))
	{
	if($row5['exp_days']<=7)
	{
		$data5='<td class="bg-danger">'.$row5['exp_days'].'</td>';
	}
    else if($row5['exp_days']<=20)
	{
		$data5='<td class="bg-warning">'.$row5['exp_days'].'</td>';
	}
	else
	{
		$data5='<td>'.$row5['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i5</td>
				<td>$row5[tno]</td>
				<td>".date('d/m/y',strtotime($row5['tax_start']))."</td>
				<td>".date('d/m/y',strtotime($row5['tax_end']))."</td>
				$data5
			</tr>";
		$i5++;		
	}	

echo "</table>";	
}
else
{
	echo "<br /><center><b>No Tax Receipt going to expire in next 20 Days</b></center>";
}
?>
					</div>
					
					
				</div>
				
			</div><!--/.col-->
	
		<div class="col-md-6 col-sm-12">
			
				<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:green;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>PUC</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$puc=mysqli_query($conn,"SELECT tno,puc_start,puc_end,DATEDIFF(`puc_end`,'$today') as exp_days FROM 
own_truck_docs_exp WHERE puc_end<='$days_ago' AND puc_end!=0 order by puc_end asc");

if(!$puc)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($puc)>0)
{
	echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
			<tr>
				<th>Id</th>
				<th>Truck No</th>
				<th>Date of Reg.</th>
				<th>Expires On</th>
				<th>Expires in Days</th>
			</tr>
		";
$i6=1;
		
	while($row6=mysqli_fetch_array($puc))
	{
	if($row6['exp_days']<=7)
	{
		$data6='<td class="bg-danger">'.$row6['exp_days'].'</td>';
	}
    else if($row6['exp_days']<=20)
	{
		$data6='<td class="bg-warning">'.$row6['exp_days'].'</td>';
	}
	else
	{
		$data6='<td>'.$row6['exp_days'].'</td>';
	}
	
		echo "<tr>
				<td>$i6</td>
				<td>$row6[tno]</td>
				<td>".date('d/m/y',strtotime($row6['puc_start']))."</td>
				<td>".date('d/m/y',strtotime($row6['puc_end']))."</td>
				$data6
			</tr>";
		$i6++;		
	}	

echo "</table>";	
}
else
{
	echo "<br /><center><b>No PUC going to expire in next 20 Days</b></center>";
}
?>
					</div>
				</div>
			</div><!--/.col-->
		</div>
		
		
	</div>
</div>
</body>
</html>