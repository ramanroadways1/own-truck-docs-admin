<?php
require_once("./connect.php");

$tno=mysqli_real_escape_string($conn,strtoupper($_POST['tno']));

$today=date("Y-m-d");

if($tno!='')
{	
$qry=mysqli_query($conn,"SELECT own_truck_docs.*,own_truck_docs_exp.* FROM own_truck_docs,own_truck_docs_exp WHERE own_truck_docs.tno='$tno' AND 
own_truck_docs.tno=own_truck_docs_exp.tno");

if(!$qry)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($qry)>0)
{
$row=mysqli_fetch_array($qry);

if($row['permit_five_start']==0)
{
	echo "<script>
		alert('Document not uploaded yet for this vehicle.');
		window.location.href='./manage.php';
	</script>";
	exit();
}

if($row['tax']=='')
{
	$tax_img=$row['tax_lifetime'];
}
else
{
	$tax_img=$row['tax'];
}

if($row['puc_start']==0)
{
	echo "<script>
			function MyFunc1()
			{
				$('#puc_button').attr('disabled',true);
			}
		</script>";
}
else
{
	echo "<script>
			function MyFunc1()
			{
				$('#puc_button').attr('disabled',false);
			}
		</script>";
}

if($row['permit_one_start']==0)
{
	echo "<script>
			function MyFunc2()
			{
				$('#permit_button').attr('disabled',true);
			}
		</script>";
}
else
{
	echo "<script>
			function MyFunc2()
			{
				$('#permit_button').attr('disabled',false);
			}
		</script>";
}

if($row['tax_start']==0)
{
	echo "<script>
			function MyFunc3()
			{
				$('#tax_button').attr('disabled',true);
			}
		</script>";
}
else
{
	echo "<script>
			function MyFunc3()
			{
				$('#tax_button').attr('disabled',false);
			}
		</script>";
}
?>
<script type="text/javascript">
$(document).ready(function (e) {
$("#updateForm").on('submit',(function(e) {
$("#loadicon").show();
e.preventDefault();
 $("#button2").attr("disabled",true);
	$.ajax({
	url: "./date_update.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result2").html(data);
	$("#loadicon").hide();
	},
	error: function() 
	{} });}));});
</script>

  <div class="modal fade" id="PucModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
        <script>	   
		function Modal(elem)
		{
			$('#div1').html(elem);
			$('#div2').val(elem);
		}
		</script>
		
		<form id="updateForm">	   
		
		<div class="modal-body">
			<div class="row">
				<div style="background:#666;color:#FFF" class="form-group col-md-12">
					<h4>Update <span id="div1"></span> Dates :</h4>
				 </div>
           
		   <input type="hidden" name="key_value" id="div2"/>
		   <input type="hidden" value="<?php echo $tno; ?>" name="tno" />
		   
		   <div class="form-group col-md-6">
               <label>Valid From <font color="red"><sup>*</sup></font> </label>
               <input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="from_date" type="date" class="form-control" required />
			</div>
			
			 <div class="form-group col-md-6">
               <label>Valid To <font color="red"><sup>*</sup></font> </label>
               <input min="2010-01-01" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" name="to_date" type="date" class="form-control" required />
			</div>
			</div>
		
	<div class="row">	
      <div class="pull-right" style="padding-right:20px">
<button type="button" id="hide_modal" class="btn btn-danger" data-dismiss="modal">Cancel</button>
&nbsp;
<input type="submit" id="button2" name="lr_sub" class="btn btn-primary" value="Update" />
	 </div>	
	 </div>
</div>
</form>
	</div>	
      </div>
		</div>
                       
	<?php
	echo "
	<br />
	<b><span style='font-family:Verdana'>Showing result of : <font color='red'><b>$tno</b></font></span></b>
	<form action='delete_truck_doc.php' method='POST'>
		<input type='hidden' name='tno' value='$tno'>
		<button class='btn btn-sm btn-danger pull-right'>Delete ALL Documents</button>
	</form>	
	<br />
	<br />
	<table class='table table-bordered' style='font-family:Verdana;font-size:12px;color:#000'>
		<tr>
			<th>Document</th>
			<th>Attached Copy</th>
			<th>Reg Date</th>
			<th>Exp on</th>
			<th>EditDates</th>
		</tr>
		
		<tr>
		
		<td>
			<label>RC Front</label>
		</td>
		<td>		
			<b><a target='_blank' href='$row[rc_front]'>View here</a></b>
		</td>
		
		<td>		
		</td>
		
		<td>		
		</td>
		
		<td>		
		</td>
		
		</tr>
		
		<tr>
		
		<td>
			<label>RC Rear</label>
		</td>
		<td>		
			<b><a target='_blank' href='$row[rc_rear]'>View here</a></b>
		</td>
		
		<td>		
		</td>
		
		<td>		
		</td>
		
		<td>		
		</td>
		
		</tr>
		
		<tr>
		
		<td>
			<label>PUC</label>
		</td>

		<td>		
			<b><a target='_blank' href='$row[puc]'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[puc_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[puc_end]' readonly class='form-control' required />
		</td>
		";
		?>
		<td>	
			<button id="puc_button" onclick="Modal('PUC')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		</tr>
		
		<tr>
		<td>
			<label>Permit (1Yr)</label>
		</td>

		<td>		
			<b><a target='_blank' href='$row[permit_one]'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[permit_one_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[permit_one_end]' readonly class='form-control' required />
		</td>
		";
		?>
		<td>	
			<button id="permit_button" onclick="Modal('PERMIT ONE YR')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		</tr>
		
		<tr>
		<td>	
			<label>Permit (5Yrs)</label>
		</td>
			
		<td>		
			<b><a target='_blank' href='$row[permit_five]'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[permit_five_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[permit_five_end]' readonly class='form-control' required />
		</td>
		
		";
		?>
		<td>	
			<button onclick="Modal('PERMIT FIVE YR')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		
		</tr>
	
		<tr>
		<td>	
			<label>Fitness</label>
		</td>
		<td>		
			<b><a target='_blank' href='$row[fitness]'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[fitness_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[fitness_end]' readonly class='form-control' required />
		</td>
		";
		?>
		<td>	
			<button onclick="Modal('FITNESS')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		</tr>
		
		<tr>
		
		<td>
			<label>Tax</label>
		</td>
		
		<td>		
			<b><a target='_blank' href='$tax_img'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[tax_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[tax_end]' readonly class='form-control' required />
		</td>
		
		";
		?>
		<td>	
			<button id="tax_button" onclick="Modal('TAX')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		
		</tr>
		
		<tr>
		
		<td>
			<label>Insurance</label>
		</td>
		
		<td>		
			<b><a target='_blank' href='$row[ins]'>View here</a></b>
		</td>
		
		<td>	
			<input type='text' value='$row[ins_start]' readonly class='form-control' required />
		</td>
		
		<td>	
			<input type='text' value='$row[ins_end]' readonly class='form-control' required />
		</td>
		";
		?>
		<td>	
			<button onclick="Modal('Insurance')" data-toggle="modal" data-target="#PucModal" type="button" class="btn-sm btn btn-danger">Edit</button>
		</td>
		<?php
		echo "
		
	</tr>
	<input type='hidden' name='tno' value='$tno'>
	
	<script>
	MyFunc1();
	MyFunc2();
	MyFunc3();
	</script>
	";
}
else
{
	echo "<script>
	alert('Invalid truck No');
	window.location.href='./manage.php';
	</script>";
	exit();
}
}
?>