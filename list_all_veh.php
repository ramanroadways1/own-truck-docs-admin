<?php
require_once 'connect.php';
$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RRPL</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 1;">
	<center><img style="margin-top:150px" src="./load.gif" /></center>
</div>

<div id="result2"></div>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
	 color:#000;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body>
<a href="./"><button class="pull-left btn btn-danger" style="margin:10px;letter-spacing:">Dashboard</button></a>
<a target="_blank" href="./download_all_veh.php"><button type="button" style="margin:10px;letter-spacing:" 
class="pull-right btn btn-primary">Download Excel</button></a>
<div class="container-fluid;font-family:Verdana">	
<br />
<br />
<br />
<div class="col-md-12">			
	
	<table class="table table-bordered" style="font-family:Verdana;font-size:13px;">
		<tr>
			<th>Id</th>
			<th>Truck No</th>
			<th>Permit(1Yr)<br>Start</th>
			<th>Permit(1Yr)<br>Ends</th>
			<th>Permit(5Yr)<br>Start</th>
			<th>Permit(5Yr)<br>Ends</th>
			<th>Fitness<br>Start</th>
			<th>Fitness<br>Ends</th>
			<th>Tax<br>Start</th>
			<th>Tax<br>Ends</th>
			<th>Insurance<br>Start</th>
			<th>Insurance<br>Ends</th>
			<th>PUC<br>Start</th>
			<th>PUC<br>Ends</th>
		</tr>
		<?php
		
		$qry=mysqli_query($conn,"SELECT o.tno,e.permit_five_start,e.permit_five_end,e.permit_one_start,e.permit_one_end,e.fitness_start,
		e.fitness_end,e.tax_start,e.tax_end,e.ins_start,e.ins_end,e.puc_start,e.puc_end 
		FROM dairy.own_truck AS o 
		LEFT OUTER JOIN rrpl_database.own_truck_docs_exp AS e ON e.tno = o.tno 
		WHERE o.is_sold!='1' ORDER BY o.tno ASC");
		
		if(mysqli_num_rows($qry)>0)
		{
			$i=1;
			while($row=mysqli_fetch_array($qry))
			{
				if($row['permit_one_start']!=0)
				{ $p1_start=date("d/m/y",strtotime($row["permit_one_start"])); }
				else { $p1_start="NULL"; }
				
				if($row['permit_one_end']!=0)
				{ $p1_end=date("d/m/y",strtotime($row["permit_one_end"])); }
				else { $p1_end="NULL"; }
				
				if($row['permit_five_start']!=0)
				{ $p5_start=date("d/m/y",strtotime($row["permit_five_start"])); }
				else { $p5_start="NULL"; }
				
				if($row['permit_five_end']!=0)
				{ $p5_end=date("d/m/y",strtotime($row["permit_five_end"])); }
				else { $p5_end="NULL"; }
				
				if($row['fitness_start']!=0)
				{ $fitness_start=date("d/m/y",strtotime($row["fitness_start"])); }
				else { $fitness_start="NULL"; }
				
				if($row['fitness_end']!=0)
				{ $fitness_end=date("d/m/y",strtotime($row["fitness_end"])); }
				else { $fitness_end="NULL"; }
				
				if($row['tax_start']!=0)
				{ $tax_start=date("d/m/y",strtotime($row["tax_start"])); }
				else { $tax_start="NULL"; }
				
				if($row['tax_end']!=0)
				{ $tax_end=date("d/m/y",strtotime($row["tax_end"])); }
				else { $tax_end="NULL"; }
				
				if($row['ins_start']!=0)
				{ $ins_start=date("d/m/y",strtotime($row["ins_start"])); }
				else { $ins_start="NULL"; }
				
				if($row['ins_end']!=0)
				{ $ins_end=date("d/m/y",strtotime($row["ins_end"])); }
				else { $ins_end="NULL"; }
				
				if($row['puc_start']!=0)
				{ $puc_start=date("d/m/y",strtotime($row["puc_start"])); }
				else { $puc_start="NULL"; }
				
				if($row['puc_end']!=0)
				{ $puc_end=date("d/m/y",strtotime($row["puc_end"])); }
				else { $puc_end="NULL"; }
				
				if($p1_end!='NULL' AND strtotime($row['permit_one_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>";
				}
				else if($p5_end!='NULL' AND strtotime($row['permit_five_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>";
				}
				else if($fitness_end!='NULL' AND strtotime($row['fitness_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>";
				}
				else if($tax_end!='NULL' AND strtotime($row['tax_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>";
				}
				else if($ins_end!='NULL' AND strtotime($row['ins_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>"; 
				}
				else if($puc_end!='NULL' AND strtotime($row['puc_end'])<=strtotime(date("Y-m-d")))
				{
					echo "<tr style='background:orange'>";
				}
				else
				{
					echo "<tr>";
				}
				echo "
						<td>$i</td>
						<td>$row[tno]</td>
						<td>$p1_start</td>
						<td>$p1_end</td>
						<td>$p5_start</td>
						<td>$p5_end</td>
						<td>$fitness_start</td>
						<td>$fitness_end</td>
						<td>$tax_start</td>
						<td>$tax_end</td>
						<td>$ins_start</td>
						<td>$ins_end</td>
						<td>$puc_start</td>
						<td>$puc_end</td>
				</tr>";
			$i++;	
			}
		}
		else
		{
			echo "<tr>
					<td colspan='2'><b><font color='red'>No records found..</b></font></td>
			</tr>";
		}
		?>		
	</table>
	
</div>

</div>
</body>
</html>