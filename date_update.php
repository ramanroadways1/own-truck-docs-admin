<?php
require_once("./connect.php");

$tno=mysqli_real_escape_string($conn,strtoupper($_POST['tno']));
$key_value=mysqli_real_escape_string($conn,strtoupper($_POST['key_value']));
$from_date=mysqli_real_escape_string($conn,strtoupper($_POST['from_date']));
$to_date=mysqli_real_escape_string($conn,strtoupper($_POST['to_date']));

$timestamp=date("Y-m-d H:i:s");

if($key_value=='PUC')
{
	$from_col="puc_start";
	$to_col="puc_end";
}
else if($key_value=='PERMIT ONE YR')
{
	$from_col="permit_one_start";
	$to_col="permit_one_end";
}
else if($key_value=='PERMIT FIVE YR')
{
	$from_col="permit_five_start";
	$to_col="permit_five_end";
}
else if($key_value=='FITNESS')
{
	$from_col="fitness_start";
	$to_col="fitness_end";
}
else if($key_value=='TAX')
{
	$from_col="tax_start";
	$to_col="tax_end";
}
else if($key_value=='Insurance')
{
	$from_col="ins_start";
	$to_col="ins_end";
}
			
$insert2 = mysqli_query($conn,"UPDATE own_truck_docs_exp SET `$from_col`='$from_date',`$to_col`='$to_date' WHERE tno='$tno'");
	
if(!$insert2)
{
	echo mysqli_error($conn);
	exit();
}	

echo "
	<script type='text/javascript'> 
		alert('UPDATE SUCCESS.');
		fetch('$tno');
		document.getElementById('hide_modal').click();
	</script>
";
?>